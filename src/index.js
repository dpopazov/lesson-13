const Koa = require('koa');
const bodyParser = require('koa-bodyparser');

const userRouter = require('./user/router');

const app = new Koa();
const serverPort = process.env.SERVER_PORT;

app.use(bodyParser());
app.use(userRouter.routes());

app.listen(serverPort, () => {
  console.log(`Server started on port ${serverPort}`);
})

const db = require('../db/models');
const messages = require("../constants");


async function getUsers() {
  try {
    return await db.user.findAll();
  } catch (err) {
    console.error(err.message);

    return err.message;
  }
}

async function createUser(user) {
  try {
   return await db.user.create(user);
  } catch (err) {
    console.error(err.message);

    return err.message;
  }
}

async function updateUser(userId, user) {
  try {

    if (!user.email || !user.first_name || !user.last_name || !user.role) {
      throw new Error(messages.VALIDATION_ERROR);
    }

    const [role] = await db.role.findAll({ where: { name: user.role } });

    if (!role) {
      throw new Error(messages.NOT_FOUND);
    }

    const [result] = await db.user.update({ ...user, role_id: role.id }, { where: { id: userId } });

    if (!result) {
      throw new Error(messages.NOT_FOUND);
    }

    const [updatedUser] = await db.user.findAll({ where: { id: userId } });

    return updatedUser;
  } catch (err) {
    console.error(err.message);

    return err;
  }
}

async function deleteUser(userId) {
  try {
    const result = await db.user.destroy({ where: { id: userId } });

    if (!result) {
      throw new Error(messages.NOT_FOUND);
    }

    return messages.DELETE_SUCCESS;
  } catch (err) {
    console.error(err.message);

    return err;
  }
}

module.exports = {
  getUsers,
  createUser,
  updateUser,
  deleteUser
};

const Router = require('koa-router');
const { handleUserDelete, handleUserGet, handleUserPatch, handleUserPost } = require('./controller');

const router = new Router({ prefix: '/user' });

router.get('/', handleUserGet);

router.post('/', handleUserPost);

router.patch('/:id', handleUserPatch);

router.delete('/:id', handleUserDelete);

module.exports = router;

const { createUser, deleteUser, getUsers, updateUser } = require('./service');
const messages = require("../constants");

async function handleUserGet(ctx) {
  ctx.body = await getUsers();
  ctx.status = 200;
}

async function handleUserPost(ctx) {
  ctx.body = await createUser(ctx.request.body);
  ctx.status = 200;
}

async function handleUserPatch(ctx) {
  const result = await updateUser(ctx.params.id, ctx.request.body);

  if (result instanceof Error) {
    switch (result.message) {
      case messages.NOT_FOUND:
        ctx.status = 404;
        break;

      case messages.VALIDATION_ERROR:
        ctx.status = 500;
        break;

      default:
        break;

    }

    ctx.body = result.message;
  } else {
    ctx.status = 200;
    ctx.body = result;
  }
}

async function handleUserDelete(ctx) {
  const result = await deleteUser(ctx.params.id);

  if (result instanceof Error) {
    ctx.status = 404;
    ctx.body = result.message;
  } else {
    ctx.status = 200;
    ctx.body = result;
  }
}

module.exports = {
  handleUserGet,
  handleUserPost,
  handleUserPatch,
  handleUserDelete
};

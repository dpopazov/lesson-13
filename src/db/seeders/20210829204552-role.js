'use strict';

const roles = [
  { name: 'admin', created_at: new Date(), updated_at: new Date() },
  { name: 'superadmin', created_at: new Date(), updated_at: new Date() },
  { name: 'customer', created_at: new Date(), updated_at: new Date() }
];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('roles', roles);
  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.bulkDelete('roles', {
      name: {
        [Op.in]: roles.map((currentRoleOnIteration) => {return currentRoleOnIteration.name;})
      }
    });
  }
};

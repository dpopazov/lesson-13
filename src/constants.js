module.exports = {
    NOT_FOUND: 'Not found',
    VALIDATION_ERROR: 'Validation error',
    DELETE_SUCCESS: 'Successfully deleted'
};
